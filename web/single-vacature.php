<?php include 'header.php';?>

<div class="single-vacatures-wrap">
	<div class="container">
		<div class="section-header">
			<div class="meta-data">
				<span class="vtype"><span class="icon-watch"></span> Freelance</span>
				<span class="loc"><span class="icon-location-marker"></span> Utrecht</span>
			</div>
			<h2>Lead developer</h2>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.</p>
			<a href="#" class="button">Solliciteer direct</a>
		</div>
	</div>
</div>

<?php include 'template-parts/slider1.php';?>

<div class="content-wrap">
	<div class="container">
		<h2>Over ons</h2>
		<p>Sundata helpt om sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>
		<p>Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut magnam aliquam quaerat voluptatem. </p>
	</div>
</div>

<div class="content-wrap">
	<div class="container">
		<h2>Over de functie</h2>
		<p>Als Lead developer bij SunData en lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut commodo consequat.</p>
		<p>Een paar jaar later, in 2012, was daar SunData. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud ullamco laboris ex ea commodo consequat.</p>
	</div>
</div>

<div class="content-wrap">
	<div class="container">
		<ul class="styled colored-li">
			<li>Als Lead developer bij SunData en lorem ipsum dolor sit amet</li>
			<li>Consectetur adipisicing elit, sed do eiusmod</li>
			<li>Tempor incididunt ut labore et dolore magna aliqua</li>
		</ul>
	</div>
</div>

<div class="features-wrap working-at pdtop60 pdbottom105">
	<div class="container">
	
		<div class="section-header">
			<h2>Werken bij Sundata</h2>
		</div>
		
		<div class="features-list clearfix">
			<div class="col-3 feature-col">
				<img src="assets/images/3.svg" alt="">
				<h4>Startup</h4>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut.</p>
			</div>
			<div class="col-3 feature-col">
				<img src="assets/images/3.svg" alt="">
				<h4>Oplossingsgericht</h4>
				<p>Et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud lorem exercitation ullamco laboris nisi.</p>
			</div>
			<div class="col-3 feature-col">
				<img src="assets/images/3.svg" alt="">
				<h4>Duurzaam</h4>
				<p>Dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore.</p>
			</div>
		</div>
		
		<a href="#" class="button">Solliciteer direct</a>
		
	</div>
</div>

<?php include 'template-parts/team.php';?>

<div class="content-wrap mgtop105">
	<div class="container">
		<h2>Jouw skills</h2>
		<p>Als Lead developer bij SunData en lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco  aliquip ex ea commodo consequat.</p>
	</div>
</div>

<div class="content-wrap">
	<div class="container">
		<ul class="styled colored-li">
			<li>Lorem ipsum dolor sit amet</li>
			<li>Consectetur adipisicing elit, sed do eiusmod</li>
			<li>Ut labore et dolore magna aliqua</li>
			<li>Ipsum dolor sit amet, consectetur adipisicing elit</li>
			<li>Sed do eiusmod tempor incididunt ut labore et dolore magna</li>
			<li>Enim ad minim veniam, quis nostrud exercitation</li>
		</ul>
	</div>
</div>

<div class="logos-wrap pdtop105 pdbottom105">
	<div class="container">
		<ul>
			<li><a href="#"><img src="assets/images/rock.png" alt=""></a></li>
			<li><a href="#"><img src="assets/images/climate.png" alt=""></a></li>
			<li><a href="#"><img src="assets/images/ans.png" alt=""></a></li>
			<li><a href="#"><img src="assets/images/UU_logo.png" alt=""></a></li>
			<li><a href="#"><img src="assets/images/logos21).png" alt=""></a></li>
			<li><a href="#"><img src="assets/images/logos22).png" alt=""></a></li>
			<li><a href="#"><img src="assets/images/logos23).png" alt=""></a></li>
			<li><a href="#"><img src="assets/images/logos24).png" alt=""></a></li>
		</ul>
	</div>
</div>

<div class="vacatures-form-wrap">
	<div class="container">
		<div class="right-panel">
			<form action="" class="contact-form">
				 <fieldset class="top-fset">
				 
					<h2>Solliciteer direct!</h2>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
					
					<div class="form-control">
						<label for="f1">Naam</label>
						<input type="text" id="f1" placeholder="Je voor- en achternaam">
					</div>
					
					<div class="form-control">
						<label for="f2">Geboortedatum</label>
						<input type="text"  id="f2" placeholder="dd-mm-jjjj">
					</div>
					
					<div class="form-control">
						<label for="f3">E-mailadres</label>
						<input type="email" id="f3" placeholder="E-mailadres">
					</div>
					
					<div class="form-control">
						<label for="f4">Je motivatie</label>
						<textarea name="" id="f4" cols="30" rows="5"  placeholder="Waarom wil je bij Sundata komen werken?"></textarea>
					</div>
					
					<div class="form-control">
						<label>Upload je CV</label>
						<label for="file" class="file-input"> <span>Kies een bestand</span>
							<input type="file" id="file" name="file">
						</label>
					</div>
					
				  </fieldset>
				  
				  <fieldset class="btm-fset">
						<div class="form-control">
							<label for="tnc" class="checkbox-input">
								<input type="checkbox" id="tnc" name="tnc">
								<span>Ik ga akkoord met de <a href="#">algemene voorwaarden</a>.</span>
							</label>
						</div>
				</fieldset>
				<fieldset class="btm-fset1">
						<div class="form-control btn-submit">
							<button class="button">Solliciteer op deze functie</button>
						</div>
				  </fieldset>
			</form>	
			
			<p>Wij gaan ten alle tijden zorgvuldig met jouw gegevens om en verkopen deze nooit door aan derden.</p>
		</div>
	</div>
</div>

<?php include 'footer1.php';?>