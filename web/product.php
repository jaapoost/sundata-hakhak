<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html;charset=utf-8">
	<meta name="keywords" content="key, words">	
	<meta name="description" content="Website description">
	<!-- Mobile Specific Metas ================================================== -->
	
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="format-detection" content="telephone=no" /> 
    <title>Project Title</title>
	
	<link rel="stylesheet" href="https://use.typekit.net/uav2skg.css">
	<link rel="stylesheet" href="assets/css/iconFonts.css" type="text/css">
	<link rel="stylesheet" href="assets/css/app.css" type="text/css">
</head>

<body>
	
<!-- start layout -->
<div id="layout">
		
	<!-- start header -->
	<div id="header-part">
		<div class="container">
			<div class="row clearfix">
				<div class="mb-bar nav-brand clearfix">
					<div class="logo-bar"><a href="index.php"><img src="assets/images/logo.svg" alt=""/></a></div>
					<div class="mail-phone">
						<a href="#" class="mail-id">info@sundata.nl</a>
						<a href="#">+31 629 575 196</a>
					</div>
					
					<a id="nav-toggle" href="javascript:void(0);"><span></span></a>
				</div>
				<div class="nav-bar nav-bar-menu">
				
					<ul class="menu">
						<li> <a href="product.php">Over Sundata</a></li>
						<li  class="current">
							<a href="over-sundata.php">Producten</a>
							<div class="mega-submenu">
								<ul>
									<li><a href="#">
											<strong class="title">Het bewoners dashboard</strong>
											<span class="arrow icon-long-next-arrow"></span>
											<img src="assets/images/98-layers.svg" alt="">
										</a>
									</li>
									<li><a href="#">
											<strong class="title">SunData Pro</strong>
											<span class="arrow icon-long-next-arrow"></span>
											<img src="assets/images/70-layers.svg" alt="">
										</a>
									</li>
								</ul>
							</div>
						</li>
						<li><a href="blog.php">Nieuws</a></li>
						<li><a href="Vacatures.php">Vacatures</a></li>
						<li><a href="contact.php">Contact</a></li>
					</ul>
					
					<ul class="mobile-menu">
						<li><a href="#">info@sundata.nl</a></li>
						<li><a href="#">+31 629 575 196</a></li>
						<li><a href="#">Algemene voorwaarden</a></li>
						<li><a href="#">Privacy</a></li>
					</ul>
					
				</div>
			</div>
		</div>

	</div>
	<!-- end header -->
	
	<div class="site-content" id="content">

<div class="circle-design z99">
	<div class="pages-banner-wrap">
		<div class="pages-banner-wrap-inner" style="background-image:url(assets/images/product-banner.png)"></div>
		<div class="container">
			<div class="text-wrap">
				<h1 class="h2">Twee producten lorem ipsum dolor sit amet consectetur sed do elit.</h1>
				<a href="#" class="button">Informatie aanvragen</a>
			</div>
		</div>
	</div>
</div>

<div class="circle-design2">
	<div class="two-half-wrap">
		<div class="container clearfix">
			<div class="col1">
				<h2>Het bewoners <br>dashboard</h2>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua ut enim ad minim veniam, quis nostrud. Amet, consectetur adipisicing. </p> 
				<p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex consequat.</p>
				
			</div>
			<div class="col2 rotate"><img src="assets/images/98-layers.svg" alt=""></div>
		</div>
	</div>

	<div class="complicated-data-wrap pdtop150 pdbottom60">
		<div class="container">
			<div class="section-header pdbottom60">
				<h2>Ingewikkelde data makkelijk weergegeven</h2>
			</div>
			
			<ul class="displaydata-wrap">
				<li class="">
					<h5><span class="icon-circele-checked"></span> SunData helpt</h5>
					<p>Een unieke oplossing speciaal voor woningcorporaties, waarmee de kosten van zonnepanelen geminimaliseerd worden en de huurdertevredenheid maximaal is.</p>
				</li>
				<li>
					<h5><span class="icon-circele-checked"></span> Overzichtelijk dashboard</h5>
					<p>Naast slimme analyses bieden we een helder overzicht van alle projecten & systemen. Hierdoor zijn rapportages up-to-date en is er altijd inzicht in prestaties van het portfolio.</p>
				</li>
				<li>
					<h5><span class="icon-circele-checked"></span> Up to date</h5>
					<p>Wij verbeteren onze producten continu zodat jij profiteert van de laatste functies en verbeteringen.</p>
				</li>
				<li>
					<h5><span class="icon-circele-checked"></span> Voor bewoners</h5>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim.</p>
				</li>
				<li>
					<h5><span class="icon-circele-checked"></span> Frisse illustraties</h5>
					<p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
				</li>
				<li>
					<h5><span class="icon-circele-checked"></span> Op maat gemaakt portaal</h5>
					<p>Met aanpasbare branding maken wij het dashboard in jouw huisstijl, zodat het vertrouwd en herkenbaar aanvoelt.</p>
				</li>
			</ul>
			
			
		</div>
	</div>
</div>
<div class="container">
	<figure class="pdtop60 pdbottom60"><img src="assets/images/slide1.png" alt=""></figure>
</div>

<div class="features-wrap pdtop105 pdbottom105">
	<div class="container">
	
		<div class="section-header">
			<h2>De voordelen van SunData</h2>
		</div>
		
		<div class="features-list clearfix">
			<div class="col-3 feature-col">
				<img src="assets/images/3.svg" alt="">
				<h4>Betrouwbaar en eerlijk</h4>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut.</p>
			</div>
			<div class="col-3 feature-col">
				<img src="assets/images/3.svg" alt="">
				<h4>Oplossingsgericht</h4>
				<p>Et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud lorem exercitation ullamco laboris nisi.</p>
			</div>
			<div class="col-3 feature-col">
				<img src="assets/images/3.svg" alt="">
				<h4>Baas over eigen data</h4>
				<p>Dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore.</p>
			</div>
		</div>
		
	</div>
</div>

<div class="tesimonial-wrap">
	<div class="container">
		<div class="testimonial-slider">
			<div class="slide">
				<div class="testimonial">
					<p>"Het bewonersdashboard gaf mij veel duidelijkheid en inzicht. Termen als kWh vind ik maar ingewikkeld. Door de vergelijking met geplante bomen en bespaarde CO2 uitstoot begreep ik het direct en het werkt ook nog eens motiverend. Een heel helder dashboard!"</p>
					<span>Tim de Jager, bewoner van SSH</span>
				</div>
			</div>
			<div class="slide">
				<div class="testimonial">
					<p>"Het bewonersdashboard gaf mij veel duidelijkheid en inzicht. Termen als kWh vind ik maar ingewikkeld. Door de vergelijking met geplante bomen en bespaarde CO2 uitstoot begreep ik het direct en het werkt ook nog eens motiverend. Een heel helder dashboard!"</p>
					<span>Tim de Jager, bewoner van SSH</span>
				</div>
			</div>
			<div class="slide">
				<div class="testimonial">
					<p>"Het bewonersdashboard gaf mij veel duidelijkheid en inzicht. Termen als kWh vind ik maar ingewikkeld. Door de vergelijking met geplante bomen en bespaarde CO2 uitstoot begreep ik het direct en het werkt ook nog eens motiverend. Een heel helder dashboard!"</p>
					<span>Tim de Jager, bewoner van SSH</span>
				</div>
			</div>
		</div>
	</div>
</div>


<div class="subscribe-wrap">
	<div class="container">
		<div class="section-header">
			<h2>Interesse of benieuwd naar de mogelijkheden?</h2>
			<p>Laat je emailadres achter, dan nemen we contact met je op en kijken we samen naar de mogelijkheden.</p>
			
			<form action="#" class="scribe-form">
				<label for=""><input type="email" placeholder="Jouw emailadres"></label>
				<button class="button">Stuur mij een e-mail</button>
			</form>
		</div>
	</div>
</div>


<div class="logos-wrap pdtop105 pdbottom150">
	<div class="container">
		<ul>
			<li><a href="#"><img src="assets/images/rock.png" alt=""></a></li>
			<li><a href="#"><img src="assets/images/climate.png" alt=""></a></li>
			<li><a href="#"><img src="assets/images/ans.png" alt=""></a></li>
			<li><a href="#"><img src="assets/images/UU_logo.png" alt=""></a></li>
			<li><a href="#"><img src="assets/images/logos21).png" alt=""></a></li>
			<li><a href="#"><img src="assets/images/logos22).png" alt=""></a></li>
			<li><a href="#"><img src="assets/images/logos23).png" alt=""></a></li>
			<li><a href="#"><img src="assets/images/logos24).png" alt=""></a></li>
		</ul>
	</div>
</div>

<?php include 'footer.php';?>