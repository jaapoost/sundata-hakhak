	</div>
	
	<!-- start footer -->
	<div id="footer-part" class="footer-only">
	
		<div class="container">
			<div class="bottom-footer">
				<ul>
					<li><a href="#">info@sundata.nl</a></li>
					<li><a href="#">+31 629 575 196</a></li>
					<li class="logo-d"><a href="#"><img src="assets/images/footer-icon.svg" alt=""></a></li>
					<li><a href="#">Algemene voorwaarden</a></li>
					<li><a href="#">Privacy</a></li>
				</ul>
			</div>
		</div>
		
	</div>
	<!-- end footer -->
		
</div>
<!-- end layout -->

<script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
<script type='text/javascript' src='https://maps.googleapis.com/maps/api/js?key=AIzaSyBhclkPskAMMxnPSlW7K9UkP4fhiY6jbd8'></script>
<script src="assets/js/slick.min.js"></script>
<script src="assets/js/main.js"></script>

</body>

</html>
