<?php include 'header.php';?>

		
<div class="blog-sec-wrap blog-page">
	<div class="container">
	
		<div class="text-wrap">
			<h2>Nieuws</h2>
		</div>
		
		<div class="blogs-list clearfix">
			<div class="blog-col">
				<a href="single-post.php"><img src="assets/images/blog1.png" alt=""></a>
				<span class="date">22-10-2018</span>
				<h6><a href="single-post.php">Monitoringoplossing SunData blijkt de oplossing voor zakelijke PV-installateurs tijdens Solar Solutions</a></h6>
			</div>
			<div class="blog-col">
				<a href="single-post.php"><img src="assets/images/blog21).png" alt=""></a>
				<span class="date">22-10-2018</span>
				<h6><a href="single-post.php">PV-onderhoud transformeert van kostenpost naar goudmijn voor new business</a></h6>
			</div>
			<div class="blog-col">
				<a href="single-post.php"><img src="assets/images/blog22).png" alt=""></a>
				<span class="date">22-10-2018</span>
				<h6><a href="single-post.php">Lorem ipsum dolor sit amet, consectetur adipisicing elit magna</a></h6>
			</div>
			<div class="blog-col">
				<a href="single-post.php"><img src="assets/images/blog1.png" alt=""></a>
				<span class="date">22-10-2018</span>
				<h6><a href="single-post.php">Monitoringoplossing SunData blijkt de oplossing voor zakelijke PV-installateurs tijdens Solar Solutions</a></h6>
			</div>
			<div class="blog-col">
				<a href="single-post.php"><img src="assets/images/blog21).png" alt=""></a>
				<span class="date">22-10-2018</span>
				<h6><a href="single-post.php">PV-onderhoud transformeert van kostenpost naar goudmijn voor new business</a></h6>
			</div>
			<div class="blog-col">
				<a href="single-post.php"><img src="assets/images/blog22).png" alt=""></a>
				<span class="date">22-10-2018</span>
				<h6><a href="single-post.php">Onduidelijkheid over serviceverantwoordelijkheden PV-systemen leidt tot 20% opbrengstverlies</a></h6>
			</div>
		</div>
		
	</div>
</div>

<?php include 'footer.php';?>