<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html;charset=utf-8">
	<meta name="keywords" content="key, words">	
	<meta name="description" content="Website description">
	<!-- Mobile Specific Metas ================================================== -->
	
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="format-detection" content="telephone=no" /> 
    <title>Project Title</title>
	
	<link rel="stylesheet" href="https://use.typekit.net/uav2skg.css">
	<link rel="stylesheet" href="assets/css/iconFonts.css" type="text/css">
	<link rel="stylesheet" href="assets/css/app.css" type="text/css">
</head>

<body>
	
<!-- start layout -->
<div id="layout">
		
	<!-- start header -->
	<div id="header-part">
		<div class="container">
			<div class="row clearfix">
				<div class="mb-bar">
					<div class="logo-bar"><a href="index.php"><img src="assets/images/logo.svg" alt=""/></a></div>
					<div class="mail-phone">
						<a href="#" class="mail-id">info@sundata.nl</a>
						<a href="#">+31 629 575 196</a>
					</div>
					<a href="javascript:void(0);" class="togglebutton"><i class="fas fa-bars"></i></a>
				</div>
				<div class="nav-bar nav-bar2">
					<a href="#" class="button">Demo aanvragen</a>
				</div>
			</div>
		</div>

	</div>
	<!-- end header -->
	
	<div class="site-content" id="content">

<div class="banner-wrap pdbottom105">
	<div class="banner-bar mask" style="background: url(assets/images/Bitmap.png)no-repeat center/cover;"> </div>
	
	<div class="container">
		<div class="text-caption">
			<span class="name-t">WONINGCORPORATIES</span>
			<h1 class="h2">Onafhankelijke monitoring van  zonne-energie.</h1>
			<p>Het platform voor woningcorporaties en bewoners. Geen wifi-problemen, dashboard in eigen huisstijl en automatische opbrengstbewaking. </p>
			<a href="#" class="button">Vraag vrijblijvend een demo aan!</a>
		</div>
	</div>
</div>
	
<div class="circle-design">
<div class="logos-wrap pdtop105">
	<div class="container">
		<span class="name-t">WIJ WERKEN SAMEN MET:</span>
		<ul>
			<li><a href="#"><img src="assets/images/rock.png" alt=""></a></li>
			<li><a href="#"><img src="assets/images/climate.png" alt=""></a></li>
			<li><a href="#"><img src="assets/images/ans.png" alt=""></a></li>
			<li><a href="#"><img src="assets/images/UU_logo.png" alt=""></a></li>
			<li><a href="#"><img src="assets/images/logos21).png" alt=""></a></li>
			<li><a href="#"><img src="assets/images/logos22).png" alt=""></a></li>
			<li><a href="#"><img src="assets/images/logos23).png" alt=""></a></li>
			<li><a href="#"><img src="assets/images/logos24).png" alt=""></a></li>
		</ul>
	</div>
</div>


<div class="warning-notes-wrap pdtop105 pdbottom105">
	<div class="container">
		<div class="section-header">
			<h2>Na het plaatsen van de zonnepanelen gaat het vaak mis.</h2>
		</div>
		<div class="warning-msgs">
			<div class="left-content">
				<p>Standaard wordt de omvormer van de zonnepanelen via de router van de klant verbonden. In de praktijk blijkt dat 50% van de omvormers binnen 1 jaar offline is. Op dat moment ontstaat er verwarring en irritatie bij bewoners. </p>
				<p>Over een levensduur van 10-25 jaar gezien, zitten de meeste onderhouds- en servicekosten in het online houden van de systemen en niet in reparatie of vervanging. </p>
			</div>
			<div class="right-content">
				<ul class="styled-warning">
					<li>De bewoner ziet geen opbrengst, maar hij betaalt wel. Hij belt de woningcorporatie boos op.</li>
					<li>De leverancier van de zonnepanelen heeft een bepaalde opbrengst beloofd. Zonder inzicht in de opbrengstdata is dit niet te controleren.</li>
					<li>Het onderhoudsbedrijf is druk met herstellen van wifi-verbindingen. Dit kost geld en tijd. </li>
					<li>Er is geen overzicht van de besparingen.</li>
				</ul>
			</div>
		</div>
	</div>
</div>
</div>

<div class="tesimonial-wrap">
	<div class="container">
		<div class="testimonial-slider">
			<div class="slide">
				<div class="testimonial">
					<p>"Het bewonersdashboard gaf mij veel duidelijkheid en inzicht. Termen als kWh vind ik maar ingewikkeld. Door de vergelijking met geplante bomen en bespaarde CO2 uitstoot begreep ik het direct en het werkt ook nog eens motiverend. Een heel helder dashboard!"</p>
					<span>Tim de Jager, bewoner van SSH</span>
				</div>
			</div>
			<div class="slide">
				<div class="testimonial">
					<p>"Het bewonersdashboard gaf mij veel duidelijkheid en inzicht. Termen als kWh vind ik maar ingewikkeld. Door de vergelijking met geplante bomen en bespaarde CO2 uitstoot begreep ik het direct en het werkt ook nog eens motiverend. Een heel helder dashboard!"</p>
					<span>Tim de Jager, bewoner van SSH</span>
				</div>
			</div>
			<div class="slide">
				<div class="testimonial">
					<p>"Het bewonersdashboard gaf mij veel duidelijkheid en inzicht. Termen als kWh vind ik maar ingewikkeld. Door de vergelijking met geplante bomen en bespaarde CO2 uitstoot begreep ik het direct en het werkt ook nog eens motiverend. Een heel helder dashboard!"</p>
					<span>Tim de Jager, bewoner van SSH</span>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="circle-design">
<div class="work-housing-wrap pdtop105 pdbottom150">
	<div class="container">
		<div class="section-header">
			<h2>Hoe werkt SunData voor woningcorporaties?</h2>
		</div>
	</div>
	
	<div class="two-half-wrap turn-lft pos-right">
		<div class="container clearfix">
			<div class="col1">
				<h3>1. Dashboard voor bewoners in huisstijl woningcorporatie</h3>
				<p>Bewoners ontvangen toegang tot hun unieke eigen dashboard. Daarop kunnen zien hoeveel energie de zonnepanelen opleveren, hoe groot het financiele voor-deel is en wat dit betekent voor het milieu. </p>
				<p>Het dashboard is speciaal ontworpen voor woning-corporaties. Het is eenvoudig te begrijpen, volledig Nederlandstalig en simpel te gebruiken. </p>
				<p>Bewoners hebben zo vooraf al meer vertrouwen in de zonnepanelen, wat zorgt voor een breder draagvlak. </p>
			</div>
			<div class="col2">
				<div class="fig figure bk-shdw-left"><figure><img src="assets/images/91-layers.png" alt=""></figure></div>
			</div>
		</div>
	</div>

	<div class="two-half-wrap turn-rght">
		<div class="container clearfix">
			<div class="col1">
				<h3>2 . Veilig: privacy & hackerproof</h3>
				<p>De onafhankelijke software en hardware van SunData is speciaal ontworpen voor woningcorporaties. Er worden geen persoonsgegevens van bewoners gebruikt. Ook de internetverbinding van de bewoner wordt niet belast. In plaats daarvan maken wij gebruik van het privacy-ongevoelige en anonieme GPRS-netwerk. </p>
				<p>Het gebruik van SunData is veilig voor bewoners en de woningcorporatie. </p>
			</div>
			<div class="col2">
				<div class="fig figure bk-shdw-right"><figure><img src="assets/images/68-layers.png" alt=""></figure></div>
			</div>
		</div>
	</div>
	
	<div class="two-half-wrap turn-lft pos-right">
		<div class="container clearfix">
			<div class="col1">
				<h3>3. Rapportage & opbrengst-afspraken</h3>
				<p>Met het SunData platform kunnen rapportages gemaakt worden op elk niveau. Per complex, postcodegebied, plaats of alle geinstalleerde systemen. In de rapportages vind je de opgewekte energie en de milieuvoordelen. </p>
				<p>Met SunData voor woningcorporaties is het mogelijk prestatieafspraken te maken en te controleren. De voorspelde opbrengst kan achteraf gecontroleerd worden. Op die manier is het zeker dat de systemen doen wat ze beloven. Want daar draait het om.  </p>
			</div>
			<div class="col2">
				<div class="fig figure bk-shdw-left"><figure><img src="assets/images/91-layers.png" alt=""></figure></div>
			</div>
		</div>
	</div>
	
</div>
</div>
<div class="form-section-wrap pdtop150 pdbottom105">
	<div class="container">
		
		<div class="right-panel">
			<form action="" class="contact-form">
				 <fieldset class="top-fset">
				 
					<div class="form-control">
						<label for="f1">Naam</label>
						<input type="text" id="f1" placeholder="Naam">
					</div>
					
					<div class="form-control">
						<label for="f2">Bedrijfsnaam</label>
						<input type="text" id="f2" placeholder="Bedrijfsnaam">
					</div>
					
					<div class="form-control">
						<label for="f3">E-mailadres</label>
						<input type="email" id="f3" placeholder="E-mailadres">
					</div>
					
				  </fieldset>
				  
				  <fieldset class="btm-fset">
						<div class="form-control">
							<label for="tnc1" class="checkbox-input">
								<input type="checkbox" id="tnc1" name="tnc1">
								<span>Ik ga akkoord met de <a href="#">algemene voorwaarden</a>.</span>
							</label>
						</div>
				</fieldset>
				<fieldset class="btm-fset1">
						<div class="form-control btn-submit">
							<button class="button">Stuur mij meer informatie</button>
						</div>
				  </fieldset>
			</form>	
			
			<p>Wij gaan ten alle tijden zorgvuldig met jouw gegevens om en verkopen deze nooit door aan derden.</p>
		</div>
		
		<div class="left-panel">
			<h2>Jouw bewoners meer inzicht geven?</h2>
			<p>Werk je voor een woningcorporatie en ga je binnenkort een zonnepanelenproject starten? Of ben je installateur die een groot woningcorporatieproject gaat installeren? Vraag direct een gratis demo aan.</p>
		</div>
		
	</div>
</div>

<div class="complicated-data-wrap pdtop150 pdbottom60">
	<div class="container">
		<div class="section-header pdbottom60">
			<h2>Ingewikkelde data makkelijk weergegeven</h2>
		</div>
		
		<ul class="displaydata-wrap">
			<li class="">
				<h5><span class="icon-circele-checked"></span> SunData helpt</h5>
				<p>Een unieke oplossing speciaal voor woningcorporaties, waarmee de kosten van zonnepanelen geminimaliseerd worden en de huurdertevredenheid maximaal is.</p>
			</li>
			<li>
				<h5><span class="icon-circele-checked"></span> Overzichtelijk dashboard</h5>
				<p>Naast slimme analyses bieden we een helder overzicht van alle projecten & systemen. Hierdoor zijn rapportages up-to-date en is er altijd inzicht in prestaties van het portfolio.</p>
			</li>
			<li>
				<h5><span class="icon-circele-checked"></span> Up to date</h5>
				<p>Wij verbeteren onze producten continu zodat jij profiteert van de laatste functies en verbeteringen.</p>
			</li>
			<li>
				<h5><span class="icon-circele-checked"></span> Voor bewoners</h5>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim.</p>
			</li>
			<li>
				<h5><span class="icon-circele-checked"></span> Frisse illustraties</h5>
				<p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
			</li>
			<li>
				<h5><span class="icon-circele-checked"></span> Op maat gemaakt portaal</h5>
				<p>Met aanpasbare branding maken wij het dashboard in jouw huisstijl, zodat het vertrouwd en herkenbaar aanvoelt.</p>
			</li>
		</ul>
		
		
	</div>
</div>

<div class="logos-wrap pdtop60 pdbottom150">
	<div class="container">
		<span class="name-t">SUNDATA IN HET NIEUWS</span>
		<ul>
			<li><a href="#"><img src="assets/images/rock.png" alt=""></a></li>
			<li><a href="#"><img src="assets/images/climate.png" alt=""></a></li>
			<li><a href="#"><img src="assets/images/ans.png" alt=""></a></li>
			<li><a href="#"><img src="assets/images/UU_logo.png" alt=""></a></li>
		</ul>
	</div>
</div>

<?php include 'footer.php';?>