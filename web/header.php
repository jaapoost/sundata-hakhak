<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html;charset=utf-8">
	<meta name="keywords" content="key, words">	
	<meta name="description" content="Website description">
	<!-- Mobile Specific Metas ================================================== -->
	
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="format-detection" content="telephone=no" /> 
    <title>Project Title</title>
	
	<link rel="stylesheet" href="https://use.typekit.net/uav2skg.css">
	<link rel="stylesheet" href="assets/css/iconFonts.css" type="text/css">
	<link rel="stylesheet" href="assets/css/app.css" type="text/css">
</head>

<body>
	
<!-- start layout -->
<div id="layout">
		
	<!-- start header -->
	<div id="header-part">
		<div class="container">
			<div class="row clearfix">
				<div class="mb-bar nav-brand clearfix">
					<div class="logo-bar"><a href="index.php"><img src="assets/images/logo.svg" alt=""/></a></div>
					<div class="mail-phone">
						<a href="#" class="mail-id">info@sundata.nl</a>
						<a href="#">+31 629 575 196</a>
					</div>
					
					<a id="nav-toggle" href="javascript:void(0);"><span></span></a>
				</div>
				<div class="nav-bar nav-bar-menu">
				
					<ul class="menu">
						<li class="current">
							<a href="product.php">Voor wie?</a>
							<ul class="submenu">
								<li><a href="landing-page.php">Landing Page</a></li>
								<li><a href="landing-page2.php">Landing Page 2</a></li>
								<li><a href="page.php">page</a></li>
								<li><a href="404-page.php">404 Page</a></li>
							</ul>
						</li>
						<li><a href="over-sundata.php">Over Sundata</a></li>
						<li><a href="blog.php">Nieuws</a></li>
						<li><a href="Vacatures.php">Vacatures</a></li>
						<li><a href="contact.php">Contact</a></li>
					</ul>
					
					<ul class="mobile-menu">
						<li><a href="#">info@sundata.nl</a></li>
						<li><a href="#">+31 629 575 196</a></li>
						<li><a href="#">Algemene voorwaarden</a></li>
						<li><a href="#">Privacy</a></li>
					</ul>
					
				</div>
			</div>
		</div>

	</div>
	<!-- end header -->
	
	<div class="site-content" id="content">