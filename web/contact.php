<?php include 'header.php';?>

<div class="circle-design z99">
	<div class="pages-banner-wrap">
		<div class="pages-banner-wrap-inner" style="background-image:url(assets/images/contact.png)"></div>
		<div class="container">
			<div class="text-wrap">
				<h1 class="h2">Ben je op zoek naar meer informatie? Neem contact met ons op! </h1>
			</div>
		</div>
	</div>
</div>

<div class="circle-design2">
<div class="contact-wrap">
	<div class="container">
		<div class="row">
			<div class="left-panel">
				<h2>Sundata BV</h2>
				
				<h5>Contact</h5>				
				<p>
					<a href="mailto:info@sundata.nl">info@sundata.nl</a>
					<span>030 123 456 78</span>
				</p>
					
				<h5>Adres</h5>
				<address>
					Brigittenstraat 22 <br>
					3512KM, Utrecht<br>
					Nederland
				</address>
				<span class="kvk">KVK 12345678</span>
				
				<div id="map"></div>
			</div>
			<div class="right-panel">
				<form class="contact-form">
					 <fieldset class="top-fset">
					 
						<h5>Hoe kunnen we je helpen?</h5>
						
						<div class="form-control">
							<label for="reden">Reden voor contact</label>
							<select name="" id="reden">
								<option value="">Informatie aanvragen</option>
								<option value="">Informatie aanvragen</option>
							</select>
						</div>
						
						<div class="form-control">
							<label for="cfname">Naam</label>
							<input type="text" id="cfname" placeholder="Je voor- en achternaam">
						</div>
						
						<div class="form-control">
							<label for="clname">Bedrijfsnaam</label>
							<input type="text" id="clname" placeholder="Je bedrijfsnaam">
						</div>
						
						<div class="form-control">
							<label for="cmail">E-mailadres</label>
							<input type="email" id="cmail" placeholder="E-mailadres">
						</div>
						
						<div class="form-control">
							<label for="tel">Telefoonnummer</label>
							<input type="text" id="tel" placeholder="Je telefoonnummer">
						</div>
						
						<div class="form-control">
							<label for="msg">Opmerkingen</label>
							<textarea name="" id="msg" cols="30" rows="5"  placeholder="Eventuele opmerkingen of vragen"></textarea>
						</div>
						
					  </fieldset>
					  
					  <fieldset class="btm-fset">
							<div class="form-control">
								<label for="tnc" class="checkbox-input">
									<input type="checkbox" id="tnc" name="tnc">
									<span>Ik ga akkoord met de <a href="#">algemene voorwaarden</a>.</span>
								</label>
							</div>
					</fieldset>
					<fieldset class="btm-fset1">
							<div class="form-control btn-submit">
								<button class="button">Versturen</button>
							</div>
					  </fieldset>
				</form>	
				
				<p>Wij gaan ten alle tijden zorgvuldig met jouw gegevens om en verkopen deze nooit door aan derden.</p>
			</div>
		</div>
	</div>
</div>
</div>

<?php include 'footer1.php';?>