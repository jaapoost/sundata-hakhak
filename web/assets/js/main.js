jQuery(function($){

	"use strict";

		$('.slider1').slick({
		  dots: false,
		  infinite: true,
		  speed: 300,
		  slidesToShow: 1,
		  centerMode: true,
		  centerPadding: '241px',
		  responsive: [
			{
			  breakpoint: 1200,
			  settings: {
				centerPadding: '120px',
			  }
			},
			{
			  breakpoint: 1024,
			  settings: {
				centerPadding: '100px',
			  }
			},
			{
			  breakpoint: 992,
			  settings: {
				centerPadding: '51px',
			  }
			},
			{
			  breakpoint: 768,
			  settings: {
				centerMode: false,
			  }
			}
		  ]
		});
		
		$('.testimonial-slider').slick({
		  dots: false,
		  infinite: true,
		  speed: 300,
		  slidesToShow: 1,
		});
		
		if( $(window).width() < 992) {
			$('.teams-member ul').slick({
			  dots: false,
			  infinite: true,
			  speed: 300,
			  slidesToShow: 2,
			  centerMode: true,
			   arrows: false,
			   responsive: [{
				  breakpoint: 1024,
				  settings: {
					slidesToShow: 2,
					slidesToScroll: 2
				  }
				},
				{
				  breakpoint: 640,
				  settings: {
					slidesToShow: 1,
					slidesToScroll: 1
				  }
				}
			  ]
			});
		}
		
		if( $(window).width() < 768) {
			$('.logos-wrap ul').slick({
			  dots: false,
			  infinite: true,
			  speed: 300,
			  slidesToShow: 2,
			  centerMode: true,
			  variableWidth:true,
			  arrows: false,
			   responsive: [{
				  breakpoint: 1024,
				  settings: {
					slidesToShow: 2,
					slidesToScroll: 2
				  }
				},
				{
				  breakpoint: 600,
				  settings: {
					slidesToShow: 1,
					slidesToScroll: 1,
				  }
				}
			  ]
			});
		}
		
		$( "#nav-toggle" ).click(function() {
			$(this).toggleClass( "active" );
			$('.nav-bar').slideToggle();
	    });
});


if(jQuery('#map').length){
	function initialize() {
		var latlng = new google.maps.LatLng(49.18589, -2.19917);

		maps = new google.maps.Map(document.getElementById('map'), {
			zoom: 14,
			center: latlng
		});

		markers = new google.maps.Marker({
			position: latlng,
			map: maps,
			icon: 'assets/images/marker.png'
		});
	}
	google.maps.event.addDomListener(window, 'load', initialize);
}