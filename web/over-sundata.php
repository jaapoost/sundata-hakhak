<?php include 'header.php';?>


<div class="pages-banner-wrap">
	<div class="pages-banner-wrap-inner" style="background-image:url(assets/images/ovr.png)"></div>
	<div class="container">
		<div class="text-wrap">
			<h1 class="h2">Het is onze missie om je vertrouwen, transparantie en controle over jouw zonnepanelen te geven.</h1>
			<a href="#" class="button">Informatie aanvragen</a>
		</div>
	</div>
</div>

<div class="about-wrap">
	<div class="container">
	
		<div class="section-header">
			<h2>Hoe werkt SunData?</h2>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris ea commodo consequat.</p>
		</div>
		
		<div class="about-design clearfix">
			<div class="left-design">
				<ul>
					<li>
						<figure><img src="assets/images/sun.svg" alt=""></figure>
					</li>
					<li>
						<figure><img src="assets/images/solar.svg" alt=""></figure>
					</li>
					<li>
						<figure><img src="assets/images/4-layers.svg" alt=""></figure>
					</li>
				</ul>
			</div>
			<div class="right-design">
				<ul>
					<li>
						<div class="img-w"><img src="assets/images/static.svg" alt=""></div>
						<div class="con-w">
							<h4>Inzicht</h4>
							<p>Feel good rapportages & scherpe analyses</p>
						</div>
					</li>
					<li>
						<div class="img-w"><img src="assets/images/monitor-mobile.svg" alt=""></div>
						<div class="con-w">
							<h4>24/7 bewaking</h4>
							<p>Overzichtelijke management dashboards</p>
						</div>
					</li>
					<li>
						<div class="img-w"><img src="assets/images/phone.svg" alt=""></div>
						<div class="con-w">
							<h4>Storingsmeldingen</h4>
							<p>Ganalyseerd op impact met oplosadvies.</p>
						</div>
					</li>
				</ul>
			</div>
		</div>
		<div class="about-content">
			<h5>We ontwikkelen proactieve monitoring oplossingen, zo halen we het meeste uit de zon. Elke dag. </h5>
			<p>Het was 2008 en lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
			<p>Een paar jaar later, in 2012, was daar SunData. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco  commodo consequat.</p>
			<p>SunData helpt om sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. </p>
		</div>
		
	</div>
</div>
	
<div class="features-wrap features-wrap1 pdtop60 pdbottom60">
	<div class="container">
	
		<div class="section-header">
			<h2>Niet alleen voor bewoners. SunData is voor iedereen.</h2>
		</div>
		
		<div class="features-list clearfix">
			<div class="col-3 feature-col">
				<img src="assets/images/4.svg" alt="">
				<h4>Woningcorporaties</h4>
				<p>Een unieke oplossing speciaal voor woningcorporaties, waarmee de kosten van zonnepanelen geminimaliseerd worden en de huurdertevredenheid maximaal is.</p>
			</div>
			<div class="col-3 feature-col">
				<img src="assets/images/5.svg" alt="">
				<h4>Projectontwikkelaar</h4>
				<p>Naast slimme analyses bieden we een helder overzicht van alle projecten & adipisicing systemen. Hierdoor zijn rapportages up-to-date en is er altijd inzicht in prestaties van het portfolio.</p>
			</div>
			<div class="col-3 feature-col">
				<img src="assets/images/6.svg" alt="">
				<h4>Installateurs</h4>
				<p>Voor installateurs verzorgen we een helder overzicht van alle geinstalleerde systemen, vroegtijdige automatische signalering bij fouten en een beheersbare serviceafdeling</p>
			</div>
		</div>
		
	</div>
</div>

<?php include 'template-parts/team.php';?>


<div class="features-wrap pdtop105 pdbottom105">
	<div class="container">
	
		<div class="section-header">
			<h2>Lorem ipsum dolor sit amet, consectetur adipisicing elit</h2>
		</div>
		
		<div class="features-list clearfix">
			<div class="col-3 feature-col">
				<img src="assets/images/3.svg" alt="">
				<h4>Betrouwbaar en eerlijk</h4>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut.</p>
			</div>
			<div class="col-3 feature-col">
				<img src="assets/images/3.svg" alt="">
				<h4>Oplossingsgericht</h4>
				<p>Et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud lorem exercitation ullamco laboris nisi.</p>
			</div>
			<div class="col-3 feature-col">
				<img src="assets/images/3.svg" alt="">
				<h4>Baas over eigen data</h4>
				<p>Dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore.</p>
			</div>
		</div>
		<a href="#" class="button button2">Bekijk onze vacatures</a>
		
	</div>
</div>

<div class="logos-about pdtop60 pdbottom150">
<?php include 'template-parts/logos.php';?>
</div>


<?php include 'footer.php';?>