<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html;charset=utf-8">
	<meta name="keywords" content="key, words">	
	<meta name="description" content="Website description">
	<!-- Mobile Specific Metas ================================================== -->
	
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="format-detection" content="telephone=no" /> 
    <title>Project Title</title>
	
	<link rel="stylesheet" href="https://use.typekit.net/uav2skg.css">
	<link rel="stylesheet" href="assets/css/iconFonts.css" type="text/css">
	<link rel="stylesheet" href="assets/css/app.css" type="text/css">
</head>

<body>
	
<!-- start layout -->
<div id="layout">
		
	<!-- start header -->
	<div id="header-part">
		<div class="container">
			<div class="row clearfix">
				<div class="mb-bar">
					<div class="logo-bar"><a href="index.php"><img src="assets/images/logo.svg" alt=""/></a></div>
					<div class="mail-phone">
						<a href="#" class="mail-id">info@sundata.nl</a>
						<a href="#">+31 629 575 196</a>
					</div>
					<a href="javascript:void(0);" class="togglebutton"><i class="fas fa-bars"></i></a>
				</div>
				<div class="nav-bar nav-bar2">
					<a href="#" class="button">Demo aanvragen</a>
				</div>
			</div>
		</div>

	</div>
	<!-- end header -->
	
	<div class="site-content" id="content">

<div class="banner-wrap pdbottom105">
	<div class="banner-bar mask" style="background: url(assets/images/Bitmap.png)no-repeat center/cover;"> </div>
	
	<div class="container">
		<div class="text-caption">
			<span class="name-t">WONINGCORPORATIES</span>
			<h1 class="h2">Onafhankelijke monitoring van  zonne-energie.</h1>
			<p>Het platform voor woningcorporaties en bewoners. Geen wifi-problemen, dashboard in eigen huisstijl en automatische opbrengstbewaking. </p>
			<a href="#" class="button">Vraag vrijblijvend een demo aan!</a>
		</div>
	</div>
</div>

<div class="circle-design">
<div class="two-half-wrap turn-lft pos-right pdtop60 pdbottom60">
	<div class="container clearfix">
	
		<div class="col2">
			<div class="fig figure bk-shdw-left"><figure><img src="assets/images/68-layers.png" alt=""></figure></div>
		</div>
		<div class="col1">
			<span class="name-t">ZONNEPANELEN VOOR PARKEN</span>
			<h2>Lorem ipsum dolor sit amet, consectetur elit</h2>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua ut enim ad minim veniam, quis nostrud exercitation.</p>
			<a href="#" class="button button2">Meer over dit product</a>
		</div>
		
	</div>
</div>

<div class="features-wrap pdtop60 pdbottom60">
	<div class="container">
	
		<div class="section-header">
			<h2>Meer inzicht in jouw zonnepanelen</h2>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua ut enim ad minim veniam, quis nostrud exercitation.</p>
		</div>
		
		<div class="features-list clearfix">
			<div class="col-3 feature-col">
				<img src="assets/images/2.svg" alt="">
				<h4>Overzichtelijk</h4>
				<p>Onze management dashboards zijn overzichtelijk en verhogen de betrokkenheid van bewoners</p>
			</div>
			<div class="col-3 feature-col">
				<img src="assets/images/1.svg" alt="">
				<h4>Begrijpelijke data</h4>
				<p>Feel good rapportages en scherpe analyses in heldere taal voor meer inzicht krijg in jouw zonnepanelen</p>
			</div>
			<div class="col-3 feature-col">
				<img src="assets/images/3.svg" alt="">
				<h4>Betrouwbaar</h4>
				<p>Geschikt voor prestatie afspraken en privacy proof hebben 24/7 beveiliging  </p>
			</div>
		</div>
		<a href="#" class="button">Contact opnemen</a>
		
	</div>
</div>
</div>

<div class="circle-design2">
<div class="form-section-wrap pdtop105 pdbottom105" style="background:none">
	<div class="container">
		
		<div class="right-panel">
			<form action="" class="contact-form" style="background:none">
				 <fieldset class="top-fset">
				 
					<div class="form-control">
						<label for="f1">Naam</label>
						<input type="text" id="f1" placeholder="Naam">
					</div>
					
					<div class="form-control">
						<label for="f2">Bedrijfsnaam</label>
						<input type="text" id="f2" placeholder="Bedrijfsnaam">
					</div>
					
					<div class="form-control">
						<label for="f3">E-mailadres</label>
						<input type="email" id="f3" placeholder="E-mailadres">
					</div>
					
				  </fieldset>
				  
				  <fieldset class="btm-fset">
						<div class="form-control">
							<label for="tnc1" class="checkbox-input">
								<input type="checkbox" id="tnc1" name="tnc">
								<span>Ik ga akkoord met de <a href="#">algemene voorwaarden</a>.</span>
							</label>
						</div>
				</fieldset>
				<fieldset class="btm-fset1">
						<div class="form-control btn-submit">
							<button class="button">Stuur mij meer informatie</button>
						</div>
				  </fieldset>
			</form>	
			
			<p>Wij gaan ten alle tijden zorgvuldig met jouw gegevens om en verkopen deze nooit door aan derden.</p>
		</div>
		
		<div class="left-panel">
			<h2>Jouw bewoners meer inzicht geven?</h2>
			<p>Werk je voor een woningcorporatie en ga je binnenkort een zonnepanelenproject starten? Of ben je installateur die een groot woningcorporatieproject gaat installeren? Vraag direct een gratis demo aan.</p>
		</div>
		
	</div>
</div>

</div>
<div class="tesimonial-wrap">
	<div class="container">
		<div class="testimonial-slider">
			<div class="slide">
				<div class="testimonial">
					<p>"Het bewonersdashboard gaf mij veel duidelijkheid en inzicht. Termen als kWh vind ik maar ingewikkeld. Door de vergelijking met geplante bomen en bespaarde CO2 uitstoot begreep ik het direct en het werkt ook nog eens motiverend. Een heel helder dashboard!"</p>
					<span>Tim de Jager, bewoner van SSH</span>
				</div>
			</div>
			<div class="slide">
				<div class="testimonial">
					<p>"Het bewonersdashboard gaf mij veel duidelijkheid en inzicht. Termen als kWh vind ik maar ingewikkeld. Door de vergelijking met geplante bomen en bespaarde CO2 uitstoot begreep ik het direct en het werkt ook nog eens motiverend. Een heel helder dashboard!"</p>
					<span>Tim de Jager, bewoner van SSH</span>
				</div>
			</div>
			<div class="slide">
				<div class="testimonial">
					<p>"Het bewonersdashboard gaf mij veel duidelijkheid en inzicht. Termen als kWh vind ik maar ingewikkeld. Door de vergelijking met geplante bomen en bespaarde CO2 uitstoot begreep ik het direct en het werkt ook nog eens motiverend. Een heel helder dashboard!"</p>
					<span>Tim de Jager, bewoner van SSH</span>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="warning-notes-wrap pdtop105 pdbottom105">
	<div class="container">
		<div class="section-header">
			<h2>Zo halen we het meeste uit<br> de zon. Elke dag. </h2>
		</div>
		<div class="warning-msgs">
			<div class="left-content">
				<p>Een paar jaar later, in 2012, was daar SunData. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
				<p>Het was 2008 en lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
			</div>
			<div class="right-content">
				<p>SunData helpt om sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. </p>
			</div>
		</div>
	</div>
</div>

<div class="content-wrap">
	<div class="container">
		<div class="section-header">
			<h2>Hoe SunData werkt</h2>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
			<a href="#" class="button">Neem contact op</a>
		</div>
	</div>
</div>

<div class="container">
	<figure class="pdtop60 pdbottom60"><img src="assets/images/23-layers.png" alt=""></figure>
</div>
<div class="logos-wrap pdtop105 pdbottom150">
	<div class="container">
		<ul>
			<li><a href="#"><img src="assets/images/rock.png" alt=""></a></li>
			<li><a href="#"><img src="assets/images/climate.png" alt=""></a></li>
			<li><a href="#"><img src="assets/images/ans.png" alt=""></a></li>
			<li><a href="#"><img src="assets/images/UU_logo.png" alt=""></a></li>
			<li><a href="#"><img src="assets/images/logos21).png" alt=""></a></li>
			<li><a href="#"><img src="assets/images/logos22).png" alt=""></a></li>
			<li><a href="#"><img src="assets/images/logos23).png" alt=""></a></li>
			<li><a href="#"><img src="assets/images/logos24).png" alt=""></a></li>
		</ul>
	</div>
</div>

<?php include 'footer.php';?>