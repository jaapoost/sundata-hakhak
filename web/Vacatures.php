<?php include 'header.php';?>


<div class="circle-design z99">
	<div class="pages-banner-wrap">
		<div class="pages-banner-wrap-inner" style="background-image:url(assets/images/Vacatures.png)"></div>
		<div class="container">
			<div class="text-wrap">
				<h1 class="h2">Werken bij SunData betekent werken aan een duurzame wereld</h1>
				<a href="contact.php" class="button">Contact opnemen</a>
			</div>
		</div>
	</div>
</div>

<div class="circle-design2">
	<div class="vacatures-wrap">
		<div class="container">
			<div class="section-header">
				<h2>Openstaande vacatures</h2>
			</div>
			
			<ul class="vacatures-list">
				<li class="vacature">
					<a href="single-vacature.php">
						<span class="name">Frontend developer</span>
						<span class="vtype"><span class="icon-watch"></span> Freelance</span>
						<span class="loc"><span class="icon-location-marker"></span> Utrecht</span>
						<span class="icon-next-arrow"></span>
					</a>
				</li>
				<li class="vacature">
					<a href="single-vacature.php">
						<span class="name">Lead Developer</span>
						<span class="vtype"><span class="icon-watch"></span> Full-time</span>
						<span class="loc"><span class="icon-location-marker"></span> Utrecht</span>
						<span class="icon-next-arrow"></span>
					</a>
				</li>
				<li class="vacature">
					<a href="single-vacature.php">
						<span class="name">Allround developer</span>
						<span class="vtype"><span class="icon-watch"></span> Part-time</span>
						<span class="loc"><span class="icon-location-marker"></span> Utrecht</span>
						<span class="icon-next-arrow"></span>
					</a>
				</li>
			</ul>
			
			<div class="section-header">
				<p>Ben je enthousiast over wat we doen en wil je deel uitmaken van ons team? We horen graag van je!</p>
				<a href="#" class="button">Contact opnemen</a>
			</div>
			
		</div>
	</div>
</div>

<?php include 'template-parts/slider1.php';?>

<?php include 'template-parts/working-at.php';?>



<?php include 'footer.php';?>