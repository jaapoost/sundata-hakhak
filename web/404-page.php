<?php include 'header.php';?>

<div class="circle-design z99">
	<div class="pages-banner-wrap">
		<div class="pages-banner-wrap-inner" style="background-image:url(assets/images/404-image.png)"></div>
		<div class="container">
			<div class="text-wrap">
				<h1 class="h2">Oeps! Het lijkt er op dat deze pagina niet (meer) bestaat.</h1>
				<a href="index.php" class="button">Naar de homepage</a>
			</div>
		</div>
	</div>
</div>

<?php include 'footer1.php';?>