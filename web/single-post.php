<?php include 'header.php';?>

<div class="global-content">

<div class="single-post-wrap">
	<div class="container">
		<div class="section-header">
			<span class="date">01-01-2019</span>
			<h2>PV-onderhoud transformeert van kostenpost naar goudmijn voor new business</h2>
		</div>
	</div>
</div>

<div class="full-image-wrp">
	<div class="container"><img src="assets/images/image-big.png" alt=""></div>
</div>

<div class="content-wrap">
	<div class="container">
		<p>Onderhoud. Servicecontracten. Elk installatiebedrijf heeft er weleens over nagedacht. Maar verder dan dat komt het vaak niet. Waarom? Omdat er veel onduidelijkheid heerst over wat pv-onderhoud precies inhoudt en wat marktconforme prijzen zijn.  Met als gevolg dat er regelmatig ontevreden klanten aan de telefoon hangen, die ze uit onmacht maar gratis bedienen. Want de installatiebedrijven willen wel dat hun klanten met een positief gevoel aan hun dienstverlening terugdenken.</p>
	</div>
</div>

<div class="content-wrap">
	<div class="container">
		<h5 style="font-size: 18px; font-weight: 500; line-height: 28px; margin-bottom:15px;">Veranderende klantvraag</h5>
		<p>Zo dacht ook de eigenaar van een installatiebedrijf in Midden-Nederland. Tot hij merkte dat hij steeds vaker de vraag kreeg om niet alleen de systemen te leveren, maar ook in het onderhoud te voorzien. <a href="#"> Onlangs kreeg hij een aanvraag </a> voor een groot systeem op een sporthal, met een onderhoudscontract. En iedere installatiespecialist weet: als het je een keer lukt om zo'n aanvraag te succesvol af te ronden, dan is de kans groot dat je herhaalaanvragen krijgt. Maar dan moet je natuurlijk wel meegenomen worden in de overweging van de potentiele klant.</p>
	</div>
</div>

<div class="blockquote-wrap">
	<div class="container">
		<blockquote class="blockquote">
			<p>Door te luisteren naar de markt en hierop te anticiperen, met behulp van een doeltreffende aanpak en slimme software, heeft het installatiebedrijf een goudmijn voor new business gecreeerd.</p>
		</blockquote>
	</div>
</div>

<div class="content-wrap">
	<div class="container">
		<h5 style="font-size: 18px; font-weight: 500; line-height: 28px; margin-bottom:15px;">Overwegingen</h5>
		<p>De eigenaar kon twee dingen doen: de offerte laten lopen, omdat onderhoud niet zijn 'core business' was. Dan riskeerde hij niet alleen deze opdracht, maar kon hij ook toekomstige installaties mislopen. Of hij kon zich over zijn bezwaren heen zetten en op zoek gaan naar een manier om aan de klantvraag te voldoen. Hij koos voor het laatste. Hij ging op zoek naar de juiste tools, waarmee hij een ijzersterk onderhoudscontract kon ontwikkelen. Dat hij, met de middelen die hij heeft, waar kan maken. Want hij is een man van zijn woord. Underpromise, daar houdt hij niet van. Gewoon doen wat je zegt en goed. Dat is zijn motto. Zijn stijl.</p>
	</div>
</div>

<div class="content-wrap">
	<div class="container">
		<ul class="styled colored-li">
			<li>Onderhoud en marktconforme prijzen</li>
			<li>Servicecontracten</li>
			<li>Installatiebedrijven in Midden-Nederland</li>
		</ul>
	</div>
</div>

<div class="full-image-wrp">
	<div class="container"><img src="assets/images/img-small.png" alt=""></div>
</div>

<div class="content-wrap">
	<div class="container">
		<h5>Onderhoudscontract</h5>
		<p>'Hoe kan ik ervoor zorgen dat ik de beloftes die ik maak, ook na kan komen?' met deze vraag kwam hij bij SunData terecht. Samen ontwikkelden de eigenaar en SunData een onderhoudscontract dat niet alleen aansloot op de aanvraag van de prospect, maar ook gebruikt kon worden als basis om onderhoud als aanvullende dienstverlening aan te bieden aan bestaande klanten.</p>
	</div>
</div>

</div>

<div class="share-post-wrap">
	<div class="container">
		<div class="share-inner">
			<a href="blog.php" class="back"><span class="icon-next-arrow"></span></a>
			<ul class="social">
				<li><a href="#"><span class="icon-fb"></span></a></li>
				<li><a href="#"><span class="icon-twitter"></span></a></li>
				<li><a href="#"><span class="icon-linkedin"></span></a></li>
				<li><a href="#"><span class="icon-mail"></span></a></li>
			</ul>
		</div>
	</div>
</div>

<?php include 'footer.php';?>