<?php include 'header.php';?>


<div class="content-wrap pdtop105 pdbottom105">
	<div class="container">
		<h1 class="h2">Algemene voorwaarden</h1>
		<p>Op deze pagina vindt u de algemene voorwaarden van sundata , zoals deze beschikbaar is gesteld door SunData . In deze algemene voorwaarden geven wij aan onder welk voorbehoud wij de informatie op onze website aan u aanbieden.</p>
		
		<h4>Intellectueel eigendom</h4>
		<p>Het gebruik van de informatie op deze website is gratis zolang u deze informatie niet kopieert, verspreidt of op een andere manier gebruikt of misbruikt. U mag de informatie op deze website alleen hergebruiken volgens de regelingen van het dwingend recht.</p>
		<p>Zonder uitdrukkelijke schriftelijke toestemming van SunData is het niet toegestaan tekst, fotomateriaal of andere materialen op deze website her te gebruiken. Het intellectueel eigendom berust bij SunData.</p>
		<p>Voor de prijzen die op onze website staan, geldt dat wij streven naar een zo zorgvuldig mogelijke weergave van de realiteit en de bedoelde prijzen. Fouten die daarbij ontstaan en herkenbaar zijn als programmeer dan wel typefouten, vormen nooit een aanleiding om een contract dan wel overeenkomst met SunData te mogen claimen of te veronderstellen.</p>
		<p>SunData streeft naar een zo actueel mogelijke website. Mocht ondanks deze inspanningen de informatie van of de inhoud op deze website onvolledig en of onjuist zijn, dan kunnen wij daarvoor geen aansprakelijkheid aanvaarden.</p>
		<p>De informatie en/of producten op deze website worden aangeboden zonder enige vorm van garantie en of aanspraak op juistheid. Wij behouden ons het recht voor om deze materialen te wijzigen, te verwijderen of opnieuw te plaatsen zonder enige voorafgaande mededeling. SunData aanvaardt geen aansprakelijkheid voor enige informatie die op websites staat waarnaar wij via hyperlinks verwijzen.</p>
	</div>
</div> 

<?php include 'footer1.php';?>