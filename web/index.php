<?php include 'header.php';?>

<div class="banner-wrap pdbottom150">
	<div class="banner-bar mask" style="background: url(assets/images/Bitmap.png)no-repeat center/cover;"> </div>
	
	<div class="container">
		<div class="text-caption">
			<span class="name-t">SUNDATA</span>
			<h1>Haal meer energie uit zonnepanelen</h1>
			<p>Door actief te monitoren, automatiseren en slimme analyses toe te passen. Deze spelen een cruciale rol in de energietransitie. </p>
			<a href="#" class="button">Vraag direct informatie aan</a>
		</div>
	</div>
</div>
	

<?php include 'template-parts/logos.php';?>

<div class="circle-design">
	<div class="two-half-wrap turn-lft small-design pdtop105 pdbottom60">
		<div class="container clearfix">
			<div class="col2">
				<div class="fig"><img src="assets/images/solar-system-2939560_1920.png" alt=""></div>
			</div>
			<div class="col1">
				<span class="name-t">SUNDATA VOOR</span>
				<h2>Woningcorporaties</h2>
				<p>Lorem ipsum dolor sit amet, consectetur adipisi cing elit, sed do eiusmod tempor incididunt.</p>			
				<a href="#" class="button button2">Ontdek de mogelijkheden</a>
			</div>
			
		</div>
	</div>
</div>	
<div class="projects-developers-wrap pdtop60 pdbottom60">
	<div class="container">
		<div class="section-header">
			<span class="name-t">SUNDATA VOOR</span>
			<h2>Projectontwikkelaars</h2>
			<p>Naast slimme analyses bieden we een helder overzicht van alle projecten & systemen. Hierdoor zijn rapportages up-to-date en is er altijd inzicht in prestaties van het portfolio.</p>
			<a href="#" class="button button2">Ontdek de mogelijkheden</a>
		</div>
	</div>
</div>

<div class="circle-design2">	
<div class="two-half-wrap turn-rght small-design pdtop60 pdbottom60">
	<div class="container clearfix">
		<div class="col1">
			<span class="name-t">SUNDATA VOOR</span>
			<h2>Installateurs</h2>
			<p>Lorem ipsum dolor sit amet, consectetur adipisi cing elit, sed do eiusmod tempor incididunt.</p>			
			<a href="#" class="button button2">Ontdek de mogelijkheden</a>
		</div>	
		<div class="col2 text-right">
			<div class="fig"><img src="assets/images/photovoltaic-2138992_1920.png" alt=""></div>		
		</div>		
	</div>
</div>
	

	
<div class="features-wrap pdtop60 pdbottom105">
	<div class="container">
	
		<div class="section-header">
			<h2>Meer inzicht in jouw zonnepanelen</h2>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua ut enim ad minim veniam, quis nostrud exercitation.</p>
		</div>
		
		<div class="features-list clearfix">
			<div class="col-3 feature-col">
				<img src="assets/images/2.svg" alt="">
				<h4>Overzichtelijk</h4>
				<p>Onze management dashboards zijn overzichtelijk en verhogen de betrokkenheid van bewoners</p>
			</div>
			<div class="col-3 feature-col">
				<img src="assets/images/1.svg" alt="">
				<h4>Begrijpelijke data</h4>
				<p>Feel good rapportages en scherpe analyses in heldere taal voor meer inzicht krijg in jouw zonnepanelen</p>
			</div>
			<div class="col-3 feature-col">
				<img src="assets/images/3.svg" alt="">
				<h4>Betrouwbaar</h4>
				<p>Geschikt voor prestatie afspraken en privacy proof hebben 24/7 beveiliging  </p>
			</div>
		</div>
		<a href="#" class="button">Contact opnemen</a>
		
	</div>
</div>
</div>	
	

		

<div class="banner-wrap inner-banner mgtop105 pdbottom60">
	<div class="banner-bar" style="background: url(assets/images/Bitmap.png)no-repeat center/cover;"> </div>
	
	<div class="container">
		<div class="text-caption">
			<span class="name-t">TEAM</span>
			<h2>We willen met SunData bijdragen aan een duurzame wereld</h2>
			<a href="#" class="button button2">Meer over ons</a>
		</div>
	</div>
</div>

		
<div class="blog-sec-wrap pdtop105 pdbottom60">
	<div class="container">
	
		<div class="text-wrap">
			<h2>Nieuws</h2>
		</div>
		
		<div class="blogs-list clearfix">
			<div class="blog-col">
				<a href="single-post.php"><img src="assets/images/blog1.png" alt=""></a>
				<span class="date">22-10-2018</span>
				<h6><a href="single-post.php">Monitoringoplossing SunData blijkt de oplossing voor zakelijke PV-installateurs tijdens Solar Solutions</a></h6>
			</div>
			<div class="blog-col">
				<a href="single-post.php"><img src="assets/images/blog21).png" alt=""></a>
				<span class="date">22-10-2018</span>
				<h6><a href="single-post.php">PV-onderhoud transformeert van kostenpost naar goudmijn voor new business</a></h6>
			</div>
			<div class="blog-col">
				<a href="single-post.php"><img src="assets/images/blog22).png" alt=""></a>
				<span class="date">22-10-2018</span>
				<h6><a href="single-post.php">Lorem ipsum dolor sit amet, consectetur adipisicing elit magna</a></h6>
			</div>
		</div>
		
	</div>
</div>

<?php include 'footer.php';?>