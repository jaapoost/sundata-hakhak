	</div>
	
	<!-- start footer -->
	<div id="footer-part">

		<div class="row top-footer">
			<div class="container">
				<div class="left-panel">
					<span class="name-t">ONTDEK MEER</span>
					<h2>Wat zijn de mogelijkheden voor jouw organisatie?</h2>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation uet dolore magna aliqua llamco.</p>
				</div>
				<div class="right-panel">
					<form class="contact-form">
						 <fieldset class="top-fset">
						 
							<div class="form-control">
								<label for="fname">Naam</label>
								<input type="text" id="fname" placeholder="Naam">
							</div>
							
							<div class="form-control">
								<label for="lname">Bedrijfsnaam</label>
								<input type="text" id="lname" placeholder="Bedrijfsnaam">
							</div>
							
							<div class="form-control not-validate">
								<label for="mail">E-mailadres</label>
								<input type="email" id ="mail" placeholder="E-mailadres">
								<span class="error-mg">Dit is geen geldig e-mailadres</span>
							</div>
							
						  </fieldset>
						  
						  <fieldset class="btm-fset">
								<div class="form-control">
									<label for="tnc" class="checkbox-input">
										<input type="checkbox" id="tnc" name="tnc">
										<span>Ik ga akkoord met de <a href="#">algemene voorwaarden</a>.</span>
									</label>
								</div>
						</fieldset>
						<fieldset class="btm-fset1">
								<div class="form-control btn-submit">
									<button class="button">Stuur mij meer informatie</button>
								</div>
						  </fieldset>
					</form>	
					
					<p>Wij gaan ten alle tijden zorgvuldig met jouw gegevens om en verkopen deze nooit door aan derden.</p>
				</div>
			</div>
		</div>
			
		<div class="bottom-footer border-top">
			<div class="container">
				<ul>
					<li><a href="#">info@sundata.nl</a></li>
					<li><a href="#">+31 629 575 196</a></li>
					<li class="logo-d"><a href="#"><img src="assets/images/footer-icon.svg" alt=""></a></li>
					<li><a href="#">Algemene voorwaarden</a></li>
					<li><a href="#">Privacy</a></li>
				</ul>
			</div>
		</div>
		
	</div>
	<!-- end footer -->
		
</div>
<!-- end layout -->

<script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
<script src="assets/js/slick.min.js"></script>
<script type='text/javascript' src='https://maps.googleapis.com/maps/api/js?key=AIzaSyBhclkPskAMMxnPSlW7K9UkP4fhiY6jbd8'></script>
<script src="assets/js/main.js"></script>
</body>

</html>
